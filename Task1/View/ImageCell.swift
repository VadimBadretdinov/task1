//
//  ImageCell.swift
//  Task1
//
//  Created by Vadim Badretdinov on 27.10.2020.
//

import UIKit

final class ImageCell: UITableViewCell {

    //MARK: - Property

    @IBOutlet weak var imageContent: UIImageView!
    @IBOutlet weak var title: UILabel!

    var operation: BlockOperation? = nil

    //MARK: - Override

    override func prepareForReuse() {
        super.prepareForReuse()

        imageContent.image = nil
        title.text = nil
        operation?.cancel()
        operation = nil
    }
}
