//
//  ImageListVC.swift
//  Task1
//
//  Created by Vadim Badretdinov on 27.10.2020.
//

import UIKit

final class ImageListVC: UIViewController {

    //MARK: - Outlets

    @IBOutlet weak var tableView: UITableView!

    //MARK: - Override

    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
    }

    //MARK: - Private

    private let queue = OperationQueue()

    private func registerCells() {
        tableView.register(UINib(nibName: "ImageCell", bundle: nil), forCellReuseIdentifier: "ImageCell")
    }

    private func downloadImage(withURL url: URL, forCell cell: UITableViewCell) -> BlockOperation? {
        let operation = BlockOperation()
        operation.addExecutionBlock { [weak cell] in
            guard let cell = cell as? ImageCell else { return }

            if let image = ImageCache.shared[url], !(cell.operation?.isCancelled ?? false) {
                DispatchQueue.main.async {
                    cell.imageContent.image = image
                }
            } else {
                URLSession.shared.dataTask(with: url, completionHandler: { (data,response,error) in
                    guard let data = data, error == nil, let image = UIImage(data: data), !(cell.operation?.isCancelled ?? false) else {
                        return
                    }
                    ImageCache.shared[url] = image
                    DispatchQueue.main.async {
                        if cell.operation == operation {
                            cell.imageContent.image = image
                        }
                    }
                }).resume()
            }
        }
        return operation
    }
}

//MARK: - UITableViewDataSource

extension ImageListVC: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        100
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as? ImageCell,
              let url = URL(string: Constants.ImageList.imgUrl + indexPath.row.stringValue),
              let operation = downloadImage(withURL: url, forCell: cell) else {
            return UITableViewCell()
        }
        cell.title.text = "Cell: \(indexPath.row)"
        cell.operation = operation
        queue.addOperation(operation)
        return cell
    }
}

//MARK: - UITableViewDelegate

extension ImageListVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        80
    }
}
