//
//  Int.swift
//  Task1
//
//  Created by Vadim Badretdinov on 27.10.2020.
//

import Foundation

extension Int {
    var stringValue: String {
        return String(self)
    }
}


