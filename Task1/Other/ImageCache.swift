//
//  ImageCache.swift
//  Task1
//
//  Created by Vadim Badretdinov on 27.10.2020.
//

import UIKit

final class ImageCache {

    //MARK: - Static

    static let shared = ImageCache()

    //MARK: - Private

    private var cache = [URL: UIImage]()

    //MARK: - subscript

    subscript(key: URL) -> UIImage? {
        get {
            return cache[key]
        }
        set (newValue) {
            cache[key] = newValue
        }
    }
}
